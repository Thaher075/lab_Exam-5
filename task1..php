<?php
class array_sort
{
    protected $__asort;

    public function __construct(array $asort)
    {
        $this->__asort = $asort;
    }

    public function mySort(){
        $sorted =$this->__asort;
        sort($sorted);
        return $sorted;
    }
}
$sortarray = new array_sort(array(11, -2, 4, 35, 0, 8, -9));


print_r($sortarray->mySort())."<br>";

?>
