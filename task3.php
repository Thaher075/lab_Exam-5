<?php
class MyCalculator {
    private $one, $two;
    public function __construct( $fval, $sval ) {
        $this->one = $fval;
        $this->two = $sval;
    }
    public function add() {
        return $this->one + $this->two;
    }
    public function subtract() {
        return $this->one - $this->two;
    }
    public function multiply() {
        return $this->one * $this->two;
    }
    public function divide() {
        return $this->one / $this->two;
    }
}
$mycalc = new MyCalculator(12, 6);
echo "Add ".$mycalc-> add()."<br>";
echo "Sub ".$mycalc-> multiply()."<br>";
echo "Mul ".$mycalc-> subtract()."<br>";
echo "Div ".$mycalc-> divide()."<br>";
?>  